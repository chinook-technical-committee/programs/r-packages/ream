##  **Location:** Remote - Multiple

## **Attendees:** Michael Folkes, David Leonard, Derek Dapp, Kris Ryding, Mark McMillan, Oliver Miler, Catarina Wor, Antonio Velez-Espino, John Carlile, Tommy Garrison, Jon Carey, Dan Auerbach, Martin Liermann, Nick Komick, Jessica Gill

## **Agenda:**
* Demonstration of github desktop for maintaining a git fork (MM)

* readCfiles, readCDSfile function updates
     *  Milestones due May 31

* Coding standards - Google R Guide
  * [Github reference](https://google.github.io/styleguide/Rguide.html)
  * [Another reference](http://adv-r.had.co.nz/Style.html)
  * [And another](https://yongjunzhang.com/posts/2018/02/google-r-style-guide/)

* Code one routine as example of standards? (CW)

* Homework

## **Discussions**
1. Mark reviewed how to use the Github desktop app to fork GitLab repositories. The tutorial can be found [here](https://gitlab.com/chinook-technical-committee/training/git_workshop/-/blob/master/exercises/Forking%20with%20Github%20Desktop.md). Forking can also be done through the git command line. Use **EXTREME** caution when merging with the CTC repository. *Make sure to submit merge requests prior to committing changes to the CTC master repository*

----
2.  Catarina updated the group on the functions to read in flat files. The first draft is complete, but a review of the functions has not been completed, so she cautioned there might be bugs in the functions. There was a discussion regarding the CIS database with the intention to combine the ERA output files and Calibration model inputs into one step, but that database isn't finalized yet. 
       
----
3.  The group reviewed coding standards to implement during the transition of vb.net code to R. During the recoding, function and variable names have kept consistent with the vb.net code in order to facilitate error checking. The group agreed that having standards in place when moving away from the recoding of the vb.net code will help with consistency among agencies and coders. Additionally, Catarina reminded the group that REAM is an R package, which allows for increased documentation, vignettes (examples), and a separate folder for R functions. 

----
4. Catarina reviewed the [vb.net code](https://gitlab.com/chinook-technical-committee/programs/coshak/-/blob/master/StartForm11.vb) for the `primarysub() which performs the Explotation Rate Analysis. Lines 246:475 are for reading in input parameters from flat files. Written into the code are points to compare to the ream outputs (e.g., line 475). After line 475, the Coshak routine occurs (CalcCohort and CalcCohortBY perform a backwards analysis based on Landed Catch to estimate cohort size, Then the program loops calculating shakers and re-estimating the cohort size until the variance in cohort size between the last pass and current pass is < 5%). 


Homework Proposal

  
- running ream on their laptops
   - fork to personal gitlab account
   - clone the package from gitlab down to laptop.
   - devs folder includes testing scripts. Set the devs folder to be the working directory. Open `user_test.R`  Run this line by line, which includes sourcing of all R function files. 
 
- review the different read flat file functions. 
   - Bugs are not likely to occur. What to look for.
<!--
-  there are a bunch of mini routines that I skipped, people could work on that!  
   - these include... 
   - Coshak FUNs that calc PNV based on stock, as per DGM can do. Catarina's hasn't translated as not currently needed. These include Sub ReadMeanLengthSD, Sub ReadMinVulnLen, Sub ReadSizeLimits, Sub CreatePNV
-->

## **Outcomes**
    *  
    *  

## **Action Items**

| Action | Who | Date |
| :--- | :------: |  ---:|
|  |  |  |
|  |  |  |

-----
### Homework update from Jessica:
Got REAM forked and cloned onto my laptop. `setwd()` to devs folder, ran lines 1:44 of "user_test.R". Hit error at line 45:
```
> A <- PrimarySub(command_data)
Read 29 items
Error in file(file, if (append) "a" else "w") : 
  cannot open the connection
In addition: Warning messages:
1: In readLines(con = command_data$cdslist$filePath) :
  incomplete final line found on '../devs/testtxt/HAR_toBY2016.cds'
2: In file(file, if (append) "a" else "w") :
  cannot open file '../devs/out/traceTHR_IDHARtraceFisheries_TermHR_HARWCBY_era.CM1.log': No such file or directory
```
If people get the same error, it's due to a missing .xslx file and a missing folder in the `devs` folder on your local drive. Reach out to Catarina or Michael for the .xlsx file and add a folder entitled "out" to your local `devs` folder. 