path <- "C:/Users/folkesm/Documents/Projects/chinook/exploitationRateAnalysis/code/coshak_sourceVB/coshak"

file <- "StartForm11.vb"
filepath <- paste(path, file, sep="/")
dat <- readLines(filepath)
dat <- trimws(dat)

sublines <- dat[grep("Sub |^Function", dat)]
sublines <- substring(sublines, 1, 45)

index.para <- sapply(gregexpr(pattern = "\\(", sublines), "[[" ,1)
sublines <- substring(sublines, 1, index.para)
sublines <- gsub(pattern = "\\(" ,replacement =  "", sublines)

#  <ul><li>- [x] item1</li><li>- [ ] item2</li></ul>
md.columns1 <- "| - [ ] |"
md.columns2 <-  "|  |  |"

md.columns1 <- gsub(" ", "", sublines)
md.columns2 <- '["'
md.columns3 <- trimws(sublines)
md.columns4 <- ']"'

md.table <- paste0(md.columns1, md.columns2, md.columns3, md.columns4)
md.table <- sort(md.table)
#md.table <- sublines
writeLines( md.table, "subsFuncslist.txt")

# sublines <- dat[grep("^Function", dat)]
# sublines <- substring(sublines, 1, 45)
# index <- grep("^End", sublines)
# sublines <- sublines[-index]
# index.para <- sapply(gregexpr(pattern = "\\(", sublines), "[[" ,1)
# sublines <- substring(sublines, 1, index.para)
# md.table <- paste(md.columns1, sublines, md.columns2)
# 
# writeLines( md.table, "subsFuncslist.txt")
