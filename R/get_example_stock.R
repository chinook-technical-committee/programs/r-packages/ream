#' Get Example Stock Input
#' 
#' Get a input data list of an example stock
#'
#' @export
#' 
get_example_stock_input <- function() {
  return(ream::dummy_stock_input[[1]])
}

#' Get Example Stock Input
#' 
#' Get a input data list of an example stock
#'
#' @export
#' 
get_example_stock_result <- function() {
  return(ream::dummy_stock_result[[1]])
}

#' Get Example Stock Command Setting
#' 
#' Get a input data list of an example stock
#'
#' @export
#' 
get_example_stock_setting <- function() {
  stock_name <- names(ream::dummy_stock_input)[1]
  return(get_def_command_setting(stock_name, 2024, NULL, TRUE))
}