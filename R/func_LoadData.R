#' Get data from CAMP
#'
#' This function queries CAMP and pulls the data needed for the ERA into a list called data_list.
#' 
#' @param command_data A list containing user input preferences
#' @param camp_conn CAMP database connection object
#' @return The stock_data list but now containing additional data from CAMP
#' @export
 
funcLoadCAMPdata <- function(command_data, camp_conn){
  
  stock <- command_data$CurrentStock
  
  # create empty list
  data_list <- list()
  
  print("Loading data")
  
  # check whether it's a superstock - used in funcCheckSuperStock inserted into  command_data$SuperStkFlg
  data_list$camp_stock <- DBI::dbGetQuery(conn = camp_conn,
                                     statement = paste0("SELECT 
                                            isSuperStock
                                        FROM camp_stock
                                        WHERE (camp_stock.Stock = '", command_data$CurrentStock,"');"))
  
  # get number of CM1 rows - inserted into command_data
  data_list$NumCM1 <- DBI::dbGetQuery(conn = camp_conn,
                               statement = paste0("SELECT COUNT(StockID)
                                     FROM ream_params
                                      WHERE (ream_params.StockID = '", command_data$CurrentStock,"');"))
  
  # get params - used in funcREAMParams
  data_list$Params <- DBI::dbGetQuery(conn = camp_conn, statement = paste0("SELECT 
                                             NetCatchAge AS TermNetSwitchAge,
                                             FirstBY, 
                                             LastBY,
                                             ProdFlag AS ProdFlg,
                                             ShakerCalcBY AS ShakCalcFlg,
                                             InterDamFlag AS InterDamFlg,
                                             EditFlag AS EditDataFlag,
                                             CompressFlag AS CompressFlg,
                                             CMBFile AS CMBFileName,
                                             ColWidth AS InputColWdth,
                                             RelReleaseFlag AS RelativeRelFlg,
                                             PrintDestFlag AS PrintDestFlg,
                                             RecovFlag AS RecovFlg,
                                             AdlEqvFlag AS AdlEqvFlg,
                                             ExpFlag AS ExpndFlg,
                                             HarvFlag AS HarvFlg,
                                             PRNTermFlag, 
                                             PRNSurvFlag,  
                                             HRJFlag,  
                                             StartAge,
                                             AvgMatRateFlag AS ReadAvgMatRteFlg,
                                             CCFFile AS AvgQFile,
                                             HRJDecimalFlag AS printHRJDecimalFlag
                                     FROM ream_params
                                      WHERE (ream_params.StockID = '", stock,"');"))
  
  # get IDL data - used in funcGetIDL only if stock_data$InterDamFlg is TRUE
  data_list$IDL <- DBI::dbGetQuery(conn = camp_conn,
                            statement = paste0("SELECT 
                                        CalendarYear,
                                        Stock,
                                        AdultSurvivalRate,
                                        JackSurvivalRate
                                     FROM ream_inter_dam_loss
                                      WHERE (ream_inter_dam_loss.Stock = '", command_data$CurrentStock,"')
                                      ORDER BY CalendarYear ASC;"))
  
  # get tag codes from the camp_cwt_release table - used in funcWireTagCode
  if(data_list$camp_stock[[1]] == FALSE | is.na(data_list$camp_stock[[1]])){
    data_list$WireTagCode <- DBI::dbGetQuery(conn = camp_conn,
                                      statement = paste0("SELECT brood_year AS [BY],
                                                     tag_code AS TagCodeList
                                              FROM camp_cwt_release
                                              WHERE (camp_cwt_release.stock = '", stock,"' 
                                              AND camp_cwt_release.included = 1) 
                                              ORDER BY brood_year;"))
                                        # 0 = not included, 1 = included
  } else { # if the stock is a superstock then you need to pull all the tag codes for the component stocks:
    data_list$WireTagCode <- DBI::dbGetQuery(conn = camp_conn,
                                      statement = paste0("SELECT camp_cwt_release.brood_year AS [BY],
                                                       camp_cwt_release.tag_code AS TagCodeList
                                               FROM camp_stock 
                                               INNER JOIN (camp_superstock_x_stock 
                                                  INNER JOIN camp_cwt_release 
                                                  ON camp_superstock_x_stock.Stock = camp_cwt_release.stock) 
                                               ON camp_stock.Stock = camp_superstock_x_stock.SuperStock
                                               WHERE (camp_stock.Stock = '", stock,"' 
                                              AND camp_cwt_release.included = 1) 
                                              ORDER BY camp_cwt_release.brood_year;"))
                                        # 0 = not included, 1 = included
  }
  
  # get number of releases - used in funcGetReleases 
  CodeList <- paste0("'", paste(noquote(data_list$WireTagCode$TagCodeList), collapse = "','"), "'")
  data_list$Releases <- DBI::dbGetQuery(conn = camp_conn,
                                 statement = paste0("SELECT brood_year AS [BY],
                                                    camp_cwt_release.[cwt_1st_mark_count] AS CWTRel1, 
                                                    camp_cwt_release.[cwt_2nd_mark_count] AS CWTRel2, 
                                                    camp_cwt_release.[non_cwt_1st_mark_count] AS NonCWTRel1, 
                                                    camp_cwt_release.[non_cwt_2nd_mark_count] AS NonCWTRel2 
                                             FROM camp_cwt_release 
                                             WHERE (((camp_cwt_release.tag_code) In (", CodeList, ")));"))
  
  # get maximum age, equivalent to line 5 of the c files for COMPLETE brood years - used in funcCWDBRecovery
  data_list$MaxAge <- DBI::dbGetQuery(conn = camp_conn,
                               statement = paste0("SELECT camp_stock.MaxAge
                                          FROM camp_stock
                                          WHERE ((camp_stock.Stock) = '", stock,"');"))
  
  # get individual recoveries by tag code - used in funcCWDBRecovery
  # NOTE that StartAge is just using the first CM1 value! 
  data_list$Recoveries <- DBI::dbGetQuery(conn = camp_conn,
                                          statement = paste0("SELECT camp_cwt_recovery.run_year, 
                                                  camp_cwt_recovery.tag_code, 
                                                  camp_cwt_recovery.age, 
                                                  SUM(camp_cwt_recovery.adjusted_estimated_number) AS adjusted_estimated_number,
                                                  camp_fishery_era.fishery_era_id, 
                                                  camp_fishery_era.name,
                                                  camp_fishery_fine.fishery_fine_id
                                          FROM ((camp_fishery_fine 
                                              INNER JOIN camp_fishery_era 
                                              ON camp_fishery_fine.fishery_era_id = camp_fishery_era.fishery_era_id) 
                                                  INNER JOIN camp_cwt_recovery 
                                                  ON camp_fishery_fine.fishery_fine_id = camp_cwt_recovery.fishery_fine_id)
                                          WHERE (((camp_cwt_recovery.tag_code) IN (", CodeList, ")) 
                                              AND ((camp_cwt_recovery.age) BETWEEN ", data_list$Params$StartAge[1], " AND ", command_data$MaxRecoveryAge ,"))
                                          GROUP BY camp_cwt_recovery.run_year, 
                                                   camp_cwt_recovery.tag_code, 
                                                   camp_cwt_recovery.age, 
                                                   camp_fishery_era.fishery_era_id, 
                                                   camp_fishery_era.name,
                                                   camp_cwt_recovery.recovery_id,
                                                   camp_fishery_fine.fishery_fine_id;"))
  
  
  # get ERA fisheries data - used in funcCAMPFisheryERA and funcREAMPNVParams
  data_list$camp_fishery_era <- DBI::dbGetQuery(conn = camp_conn,
                                       statement = paste0("SELECT *
                                                    FROM camp_fishery_era;"))
  data_list$num_era_fisheries <- as.double(length(data_list$camp_fishery_era$name[data_list$camp_fishery_era$name != "ESCAPEMENT"]))
  
  # get the PNV parameters - used in funcREAMPNVParams
  data_list$ream_pnv_params <- DBI::dbGetQuery(conn = camp_conn,
                                      statement = paste0("SELECT *
                                                 FROM ream_pnv_params;"))
  
  # get PNV data - used in funcREAMPNVData
  data_list$ream_pnv_data <- DBI::dbGetQuery(conn = camp_conn,
                                    statement = paste0("SELECT * 
                                                 FROM ream_pnv_data;"))

  # get CNR data - used in funcREAMPNVData
  data_list$ream_cnr_data <- DBI::dbGetQuery(conn = camp_conn,
                                    statement = paste0("SELECT * 
                                                  FROM ream_cnr_data;"))
  
  
  # get catch coefficient data for CCF stock because catch coefficients can be 
  # used from a proxy stock and not necessarily command_data$CurrentStock - used 
  # in funcREAMPNVData
  
  # NOTE we're doing this twice to get the two CM1 rows
  if(data_list$Params$ShakCalcFlg[1] == 0){
    # by brood year
    data_list$ream_catch_coefficients[[1]] <- DBI::dbGetQuery(conn = camp_conn,
                                                statement = paste0("SELECT * 
                                                             FROM ream_catch_coefficients
                                                             WHERE (ERAStock = '", data_list$Params$AvgQFile[1],"' 
                                                             AND ShakerMethod = 'B');"))
  } else if(data_list$Params$ShakCalcFlg[1] == 1){
    # by calendar year
    data_list$ream_catch_coefficients[[1]] <- DBI::dbGetQuery(conn = camp_conn,
                                                statement = paste0("SELECT * 
                                                             FROM ream_catch_coefficients
                                                             WHERE (ERAStock = '", data_list$Params$AvgQFile[1],"' 
                                                             AND ShakerMethod = 'C');"))
  }
  
  if(data_list$Params$ShakCalcFlg[2] == 0){
    # by brood year
    data_list$ream_catch_coefficients[[2]] <- DBI::dbGetQuery(conn = camp_conn,
                                                        statement = paste0("SELECT * 
                                                             FROM ream_catch_coefficients
                                                             WHERE (ERAStock = '", data_list$Params$AvgQFile[2],"' 
                                                             AND ShakerMethod = 'B');"))
  } else if(data_list$Params$ShakCalcFlg[2] == 1){
    # by calendar year
    data_list$ream_catch_coefficients[[2]] <- DBI::dbGetQuery(conn = camp_conn,
                                                        statement = paste0("SELECT * 
                                                             FROM ream_catch_coefficients
                                                             WHERE (ERAStock = '", data_list$Params$AvgQFile[2],"' 
                                                             AND ShakerMethod = 'C');"))
  }
  
  # get MRR and URK table if it is in CAMP, otherwise make a blank one
  if("ream_msf_fishery_release_rate" %in% dbListTables(camp_conn)){
    data_list$ream_msf_fishery_release_rate <- DBI::dbGetQuery(conn = camp_conn,
                                                    statement = paste0("SELECT * 
                                                             FROM ream_msf_fishery_release_rate;"))
    if(nrow(data_list$ream_msf_fishery_release_rate) == 0){
      data_list$ream_msf_fishery_release_rate <- data.frame(FisheryID = NA,
                                                 Year = NA,
                                                 UKR = NA,
                                                 MRR = NA,
                                                 Comments = NA)
    }
  } else {
    data_list$ream_msf_fishery_release_rate <- data.frame(FisheryID = NA,
                                               Year = NA,
                                               UKR = NA,
                                               MRR = NA,
                                               Comments = NA)
  }
  
  # get fishery translation for weighting MRR and URK
  data_list$FisheryTranslation <- DBI::dbGetQuery(conn = camp_conn,
                                                  statement = paste0("SELECT camp_fishery_fine.fishery_fine_id, 
                                                                      camp_fishery_fine.name, 
                                                                      camp_fishery_era.fishery_era_id
                                                              FROM camp_fishery_fine 
                                                              INNER JOIN camp_fishery_era 
                                                              ON camp_fishery_era.fishery_era_id = camp_fishery_fine.fishery_era_id;"))
  
  print("Done loading data")
  
  return(data_list)
  
}
